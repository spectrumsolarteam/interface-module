/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "can.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdbool.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

CAN_TxHeaderTypeDef TxHeader;
CAN_RxHeaderTypeDef RxHeader;

uint8_t TxData[8];
uint8_t RxData[8];

uint32_t TxMailbox;

uint16_t readValue = 2048;
uint16_t prevReadValue = 2048;
int16_t sendValue;
int16_t prevSendValue;

bool bilgepump = false;
bool coolantpump = false;
bool slowmode = false;

unsigned long lastButtonInterrupt = 0;
unsigned long currentTick = 0;

const int16_t READVAL_FORWARD_START = 2248;
const int16_t READVAL_FORWARD_END = 3570;
const int16_t READVAL_BACKWARD_START = 2158;
const int16_t READVAL_BACKWARD_END = 900;

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	// BILGE PUMP BUTTON
//	if(GPIO_Pin == Mag_B8_Pin) {
//		currentTick = HAL_GetTick();
//		if((currentTick - lastButtonInterrupt) > 150) {
//			lastButtonInterrupt = currentTick;
//			// this is the callback function for the button connected to Mag_B8
//			if(bilgepump) {
//				// bilge is on, so turn it off
//				HAL_GPIO_WritePin(Mag_K1_GPIO_Port, Mag_K1_Pin, GPIO_PIN_RESET); // turn off relay
//				HAL_GPIO_WritePin(Mag_B9_GPIO_Port, Mag_B9_Pin, GPIO_PIN_RESET); // turn off button light
//				bilgepump = false;
//			} else {
//				// bilge is off, so turn it on
//				HAL_GPIO_WritePin(Mag_K1_GPIO_Port, Mag_K1_Pin, GPIO_PIN_SET); // turn on relay
//				HAL_GPIO_WritePin(Mag_B9_GPIO_Port, Mag_B9_Pin, GPIO_PIN_SET); // turn on button light
//				bilgepump = true;
//			}
//		}
//	}

	// COOLANT PUMP BUTTON
	if(GPIO_Pin == Mag_A2_Pin) {
		currentTick = HAL_GetTick();
		if((currentTick - lastButtonInterrupt) > 350) {
			lastButtonInterrupt = currentTick;
			// this is the callback function for the button connected to Mag_A2
			if(coolantpump) {
				// bilge is on, so turn it off
				HAL_GPIO_WritePin(Mag_K1_GPIO_Port, Mag_K1_Pin, GPIO_PIN_RESET); // turn off relay
				HAL_GPIO_WritePin(Mag_T2B0_GPIO_Port, Mag_T2B0_Pin, GPIO_PIN_RESET); // turn off button light
				coolantpump = false;
			} else {
				// bilge is off, so turn it on
				HAL_GPIO_WritePin(Mag_K1_GPIO_Port, Mag_K1_Pin, GPIO_PIN_SET); // turn on relay
				HAL_GPIO_WritePin(Mag_T2B0_GPIO_Port, Mag_T2B0_Pin, GPIO_PIN_SET); // turn on button light
				coolantpump = true;
			}
		}
	}

	// SLOW MODE BUTTON
	if(GPIO_Pin == Mag_A4_Pin) {
		currentTick = HAL_GetTick();
		if((currentTick - lastButtonInterrupt) > 350) {
			lastButtonInterrupt = currentTick;
			// this is the callback function for the button connected to Mag_A4

			// we only allow to switch between slow mode and non-slow mode when the throttle is currently in neutral position
			if(readValue > 1995 && readValue < 2225) {
				if(slowmode) {
					// slowmode is on, so turn it off
					HAL_GPIO_WritePin(Mag_T3B7_GPIO_Port, Mag_T3B7_Pin, GPIO_PIN_RESET); // turn off button light
					slowmode = false;
				} else {
					// slowmode is off, so turn it on
					HAL_GPIO_WritePin(Mag_T3B7_GPIO_Port, Mag_T3B7_Pin, GPIO_PIN_SET); // turn on button light
					slowmode = true;
				}
			}
		}
	}
}

int16_t mapRange(uint16_t val, int16_t src_min, int16_t src_max, int16_t dst_min, int16_t dst_max) {
	return (int) ((((double)val - (double)src_min) / ((double)src_max - (double)src_min)) * ((double)dst_max - (double)dst_min) + (double)dst_min);
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_CAN_Init();
  MX_ADC1_Init();
  /* USER CODE BEGIN 2 */

  HAL_CAN_Start(&hcan);
  HAL_ADC_Start(&hadc1);

  TxHeader.DLC = 2;  // data length
  TxHeader.IDE = CAN_ID_STD;
  TxHeader.RTR = CAN_RTR_DATA;
  TxHeader.StdId = 0xC9;  // ID

  // turn off internal led
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  HAL_ADC_PollForConversion(&hadc1,1000);
	  readValue = HAL_ADC_GetValue(&hadc1);

	  if((readValue > 3890 && prevReadValue < 3600) ||
		  (readValue < 210 && prevReadValue > 495)) {
		  // in this case, the readValue was abruptly high, we consider this situation wrong, for example when a cable broke. For safety, set throttle to 0
		  readValue = 2048;
		  sendValue = 0;
	  } else {
		  prevReadValue = readValue;

		  if(readValue > READVAL_FORWARD_START) { // forward
			  sendValue = mapRange(
						  readValue,
						  READVAL_FORWARD_START,
						  READVAL_FORWARD_END,
						  0,
						  slowmode ? 300 : 1024
						);
		  } else if(readValue < READVAL_BACKWARD_START) { // backward
			  sendValue = mapRange(
						  readValue,
						  READVAL_BACKWARD_START,
						  READVAL_BACKWARD_END,
						  0,
						  slowmode ? -300 : -1023
						);
		  } else { // neutral
			  sendValue = 0;
		  }

		  // round throttle value to zero when it's small
		  // if(-30 < sendValue && sendValue < 30) sendValue = 0;

		  if((prevSendValue > 0 && sendValue < 0) || (prevSendValue < 0 && sendValue > 0)) {
			  // in this case, the throttle switched from forward to backward (or vice versa). We need to send a "0" in between so that the motor controller is happy, and does not refuse to go backwards
			  TxData[0] = 0;
			  TxData[1] = 0;
			  HAL_CAN_AddTxMessage(&hcan, &TxHeader, TxData, &TxMailbox);
			  HAL_Delay(150);
		  }

		  if ((prevSendValue - sendValue) > -5 && (prevSendValue - sendValue) < 5) {
			  // sendValue has no 'serious' difference, so 'smoothen' it out by setting the sendValue to prevSendValue
			  sendValue = prevSendValue;
		  } else {
			  prevSendValue = sendValue;
		  }
	  }

	  TxData[0] = sendValue & 0xff;
	  TxData[1] = (sendValue >> 8) & 0xff;
	  HAL_CAN_AddTxMessage(&hcan, &TxHeader, TxData, &TxMailbox);

	  HAL_Delay(150);

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV2;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
