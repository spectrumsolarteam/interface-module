# README #
We use this on a "blue pill" (STM32F103C8T6) for:

* sending CAN messages for throttle to motor controller
* turning on and off the coolant pump

### More info soon
More information will be added soon

### License
This project is licensed under GNU GPLv3